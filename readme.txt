Run the following line:
python3 MPAssignment.py

Explanation of files:
MPAssignment.py - where these scripts run and check the current directory
getLabel.py - provides segmentation function to identify the location of a getLabel
colorDetect.py - Detects colour in both the top and bottom segments
classIdentifier.py - Identifies and returns the class number
symbol.py - Responsible for symbol detection
textReader.py - Segments out characters, performs OCR and determines string

Models Directory:
Contains the dictionary and Models for my text, number and symbol recognition.
NOTE that the TextTrainer.py file is placed in their purely for documentation symbol
you can see how I trained my SVM. This code is not operational and purely acting as 
documentation.

